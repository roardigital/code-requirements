# Code Requirements #
These are the requirements the quality of any code produced by Roar Digital.
"Must" statements are hard and objective requirements which must be satisfied. "Should" statements are more subjective and should be followed where reasonably possible. 

## TDD ##
To the extent that it is possible, code should be written using test-driven development using the following discipline.

### TDD Process ##
- Write no more of a unit test than sufficient to fail (compilation failures are failures).
- Write production code only to pass a failing unit test. You should not abstract or refactor code while the test is failing. 
- Write no more production code than necessary to pass the one failing unit test.
- After the unit test passes, code must then be refactored to rest of the coding standards and keep the test passing.
- Business rules must be written with TDD and have 100% unit test coverage

### Unit tests ###
- Must assert something
- Must test only one behaviour
- Must adhere to normal coding standards
- Should abstract details of code execution so that test is mostly behaviour definition
- Unit tests must not test the details of implementation. Only a module's public API must be tested. 

## Naming and Structure ##
### Length ###
- Functions must not be longer than 20 lines
- Classes must not be longer than 100 lines
- Lines must not exceed the width of the screen
- Files should generally be less than 120 lines

### Naming ###
- Function name length should be inversely proportional to scope (large scope, small name)
- Variable name length should be proportional to scope (large scope, large name)
- Function names should be verbs 
- Variable names should be nouns
- Names should be meaningful (tell you about the system you're in)

### Cleanliness ###
- Functions must only do one thing
  - This means you cannot meaningfully extract another function from it
- Bodies of loops, branches, and functional methods (e.g. .map) should be 1-2 lines max
- Variables within a function should be at a similar level of abstraction
- Only if the code cannot explain itself should you write a comment 
  - Comment should cover intent, reasoning, consequences of change
- Bodies of loops, branches, and functions must not contain nested blocks
  - eg nested loops, nested if statements etc. 

### Code Structure / Architecture ###
- Typescript should be used as often as possible
- Business rules should be in their own module
- Business rules must not depend on any framework
- Business rules should follow the SOLID principles
- Routes should end by calling one business rule
- IO Interfaces must be injected as dependencies with an abstraction layer

## Enforcing standards ##
- It is expected that all rules are followed
- Pairing programming should be used over code reviews where possible
- Pairing should be proactively requested when there are a lot of likely subjective decisions with a broad scope
	- e.g. designing APIs, file structure for new apps, substantial refactors / new features
- On existing code bases, any added or edited code should be written following these rules where reasonably possible
- Code will not be accepted to production if it does not conform to these rules. 